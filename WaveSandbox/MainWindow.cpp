#include "MainWindow.h"
#include <QtCharts/QChartView>
#include "WaveUtils.h"
#include <QtCharts/QChart>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);
}

void MainWindow::on_actionGenerate_released()
{
    QtCharts::QChart* chart1 = new QtCharts::QChart();
    QtCharts::QChart* chart2 = new QtCharts::QChart();
    QtCharts::QChart* chart3 = new QtCharts::QChart();
    QtCharts::QChart* chartsum = new QtCharts::QChart();

    wave1 = WaveUtils::generateWave(amplitude1->text().toDouble(),
                                                 freq1->text().toDouble(),
                                                 time1->text().toDouble());
    wave2 = WaveUtils::generateWave(amplitude2->text().toDouble(),
                                                 freq2->text().toDouble(),
                                                 time2->text().toDouble());
    wave3 = WaveUtils::generateWave(amplitude3->text().toDouble(),
                                                 freq3->text().toDouble(),
                                                 time3->text().toDouble());
    chart1->addSeries(wave1);
    chart1->legend()->hide();
    chart1->createDefaultAxes();
    this->wavechart1->setChart(chart1);

    chart2->addSeries(wave2);
    chart2->legend()->hide();
    chart2->createDefaultAxes();
    this->wavechart2->setChart(chart2);

    chart3->addSeries(wave3);
    chart3->legend()->hide();
    chart3->createDefaultAxes();
    this->wavechart3->setChart(chart3);

    wavesum = WaveUtils::sumWaves(wave1, wave2, wave3);

    chartsum->addSeries(wavesum);


    chartsum->legend()->hide();
    chartsum->createDefaultAxes();
    this->wavechartsum->setChart(chartsum);

}
