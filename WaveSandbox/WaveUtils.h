#ifndef WAVEUTILS_H
#define WAVEUTILS_H
#include <QLineSeries>


class WaveUtils
{
public:
    WaveUtils();

    static QtCharts::QLineSeries* generateWave(double amplitude, double frequency, double time);
    static QtCharts::QLineSeries* sumWaves(QtCharts::QLineSeries* w1,QtCharts::QLineSeries* w2,QtCharts::QLineSeries* w3);

};

#endif // WAVEUTILS_H
