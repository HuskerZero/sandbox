#include "WaveUtils.h"
#include <QDebug>
#include <math.h>

WaveUtils::WaveUtils()
{

}

QtCharts::QLineSeries* WaveUtils::generateWave(double amplitude, double frequency, double time)
{
    qDebug() << "Generating series for: " << "amplitude - " << amplitude << " | frequency - " << frequency << " | time - " << time;

    QtCharts::QLineSeries* series = new QtCharts::QLineSeries();

    for(double i = 0; i <= time; i += 0.01)
    {
        double y = amplitude * std::sin((2 * 3.14 * frequency) * i);
        series->append(i, y);
        qDebug() << "x: " << i << " y: " << y;
    }

    return series;
}

QtCharts::QLineSeries *WaveUtils::sumWaves(QtCharts::QLineSeries *w1, QtCharts::QLineSeries *w2, QtCharts::QLineSeries *w3)
{
    auto vw1 = w1->pointsVector();
    auto vw2 = w2->pointsVector();
    auto vw3 = w3->pointsVector();

    QtCharts::QLineSeries *series = new QtCharts::QLineSeries();

    double x = 0.0;
    double y = 0.0;

    for(int i = 1; i < vw1.length(); i++)
    {
        x = vw1.at(i).x();
        y = vw1.at(i).y() + vw2.at(i).y() + vw3.at(i).y();
        series->append(x, y);
    }

    return series;
}
