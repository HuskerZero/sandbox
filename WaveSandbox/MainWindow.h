#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_MainWindow.h"
#include <QtCharts/QLineSeries>

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
private slots:
    void on_actionGenerate_released();

private:
    QtCharts::QLineSeries* wave1 = nullptr;
    QtCharts::QLineSeries* wave2 = nullptr;
    QtCharts::QLineSeries* wave3 = nullptr;
    QtCharts::QLineSeries* wavesum = nullptr;

};

#endif // MAINWINDOW_H
